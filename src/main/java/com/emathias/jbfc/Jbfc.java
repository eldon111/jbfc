package com.emathias.jbfc;

import org.apache.commons.lang3.StringUtils;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.Stack;

import static org.objectweb.asm.Opcodes.AALOAD;
import static org.objectweb.asm.Opcodes.ACC_PRIVATE;
import static org.objectweb.asm.Opcodes.ACC_PUBLIC;
import static org.objectweb.asm.Opcodes.ACC_STATIC;
import static org.objectweb.asm.Opcodes.ACC_SUPER;
import static org.objectweb.asm.Opcodes.ALOAD;
import static org.objectweb.asm.Opcodes.ARRAYLENGTH;
import static org.objectweb.asm.Opcodes.BALOAD;
import static org.objectweb.asm.Opcodes.BASTORE;
import static org.objectweb.asm.Opcodes.DUP;
import static org.objectweb.asm.Opcodes.DUP2;
import static org.objectweb.asm.Opcodes.GETFIELD;
import static org.objectweb.asm.Opcodes.GETSTATIC;
import static org.objectweb.asm.Opcodes.GOTO;
import static org.objectweb.asm.Opcodes.I2B;
import static org.objectweb.asm.Opcodes.I2C;
import static org.objectweb.asm.Opcodes.IADD;
import static org.objectweb.asm.Opcodes.ICONST_0;
import static org.objectweb.asm.Opcodes.ICONST_1;
import static org.objectweb.asm.Opcodes.IFEQ;
import static org.objectweb.asm.Opcodes.IFGT;
import static org.objectweb.asm.Opcodes.IF_ICMPEQ;
import static org.objectweb.asm.Opcodes.IF_ICMPNE;
import static org.objectweb.asm.Opcodes.ILOAD;
import static org.objectweb.asm.Opcodes.INVOKESPECIAL;
import static org.objectweb.asm.Opcodes.INVOKESTATIC;
import static org.objectweb.asm.Opcodes.INVOKEVIRTUAL;
import static org.objectweb.asm.Opcodes.ISTORE;
import static org.objectweb.asm.Opcodes.ISUB;
import static org.objectweb.asm.Opcodes.NEW;
import static org.objectweb.asm.Opcodes.NEWARRAY;
import static org.objectweb.asm.Opcodes.POP;
import static org.objectweb.asm.Opcodes.PUTFIELD;
import static org.objectweb.asm.Opcodes.RETURN;
import static org.objectweb.asm.Opcodes.SIPUSH;
import static org.objectweb.asm.Opcodes.T_BYTE;
import static org.objectweb.asm.Opcodes.V1_5;

public class Jbfc {

	private ClassWriter cw;
	private Stack<Label> labelStack = new Stack<>();
	private String className;
	private final String owner;
	private final char[] input;
	private final String classPath;

	public static void main(String[] args) {
		if (args.length == 0) {
			return;
		}
		Jbfc jbfc = new Jbfc(args);
		jbfc.compile();
	}

	public Jbfc(String... args) {
		String input = "";
		for (String arg : args) {
			System.out.println("Processing arg: " + arg);
			if (arg.matches("-c=[^\\s]*")) {
				className = arg.split("=")[1].replaceAll("\\.", "/");
				System.out.println("Setting class name: " + className);
			} else {
				File file = new File(arg);
				if (file.exists()) {
					input = readInputFile(file);
					if (StringUtils.isEmpty(className)) {
						className = arg.split("\\.[^\\.]*$")[0];
						System.out.println("Setting class name: " + className);
					}
				} else {
					input = StringUtils.join(arg.split("[^<>+\\-\\[\\].,]+"));
				}
			}
		}

		if (StringUtils.isEmpty(className)) {
			className = "BF" + System.currentTimeMillis();
			System.out.println("Setting class name: " + className);
		}

		System.out.println("Compiling: " + input);
		classPath = System.getProperty("user.dir");
		this.owner = className;
		this.input = input.toCharArray();
	}

	public String readInputFile(File file) {
		String retVal = "";
		try {
			Scanner scanner = new Scanner(file);
			while (scanner.hasNextLine()) {
				retVal += scanner.nextLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return retVal;
	}

	private void compile() {
		init();

		MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "run", "()V", null, null);
		mv.visitCode();

		for (char c : input) {
			switch (c) {
				case '<':
					mv.visitVarInsn(ALOAD, 0);
					mv.visitMethodInsn(INVOKEVIRTUAL, owner, "left", "()V");
					break;
				case '>':
					mv.visitVarInsn(ALOAD, 0);
					mv.visitMethodInsn(INVOKEVIRTUAL, owner, "right", "()V");
					break;
				case '+':
					mv.visitVarInsn(ALOAD, 0);
					mv.visitMethodInsn(INVOKEVIRTUAL, owner, "add", "()V");
					break;
				case '-':
					mv.visitVarInsn(ALOAD, 0);
					mv.visitMethodInsn(INVOKEVIRTUAL, owner, "sub", "()V");
					break;
				case '[':
					Label begin = new Label();
					Label end = new Label();
					labelStack.push(end);
					labelStack.push(begin);
					labelStack.push(begin);
					labelStack.push(end);
					begin(mv);
					break;
				case ']':
					end(mv);
					break;
				case '.':
					mv.visitVarInsn(ALOAD, 0);
					mv.visitMethodInsn(INVOKEVIRTUAL, owner, "print", "()V");
					break;
				case ',':
					mv.visitVarInsn(ALOAD, 0);
					mv.visitMethodInsn(INVOKEVIRTUAL, owner, "read", "()V");
					break;
				default: break;
			}
		}

		mv.visitInsn(RETURN);
		mv.visitMaxs(6, 6);
		mv.visitEnd();

		finish();
	}

	private void init() {
		cw = new ClassWriter(0);
		cw.visit(V1_5, ACC_PUBLIC + ACC_SUPER, owner, null, "java/lang/Object", null);

		// declare data variables
		cw.visitField(ACC_PRIVATE, "data", "[B", null, null);
		cw.visitEnd();
		cw.visitField(ACC_PRIVATE, "newline", "[B", null, null);
		cw.visitEnd();
		cw.visitField(ACC_PRIVATE, "pos", "I", null, null);
		cw.visitEnd();

		// constuctor, initialize data variables
		{
			MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(Ljava/lang/String;)V", null, null);
			mv.visitCode();

			Label l0 = new Label();
			Label l1 = new Label();
			Label l2 = new Label();
			mv.visitTryCatchBlock(l0, l1, l2, "java/io/UnsupportedEncodingException");

			mv.visitVarInsn(ALOAD, 0);
			mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V");

			mv.visitVarInsn(ALOAD, 0);
			mv.visitLdcInsn("line.separator");
			mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "getProperty", "(Ljava/lang/String;)Ljava/lang/String;");
			mv.visitLdcInsn("UTF-8");
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "getBytes", "(Ljava/lang/String;)[B");
			mv.visitFieldInsn(PUTFIELD, owner, "newline", "[B");

			mv.visitVarInsn(ALOAD, 0);
			mv.visitInsn(ICONST_0);
			mv.visitFieldInsn(PUTFIELD, owner, "pos", "I");

			mv.visitLabel(l0);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitIntInsn(SIPUSH, 30000);
			mv.visitIntInsn(NEWARRAY, T_BYTE);
			mv.visitFieldInsn(PUTFIELD, owner, "data", "[B");

			mv.visitVarInsn(ALOAD, 1);
			mv.visitLdcInsn("UTF-8");
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "getBytes", "(Ljava/lang/String;)[B");
			mv.visitInsn(ICONST_0);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, owner, "data", "[B");
			mv.visitInsn(ICONST_0);
			mv.visitVarInsn(ALOAD, 1);
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "length", "()I");
			mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "arraycopy", "(Ljava/lang/Object;ILjava/lang/Object;II)V");

			mv.visitLabel(l1);
			Label l3 = new Label();
			mv.visitJumpInsn(GOTO, l3);
			mv.visitLabel(l2);
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/UnsupportedEncodingException", "printStackTrace", "()V");
			mv.visitLabel(l3);

			mv.visitInsn(RETURN);
			mv.visitMaxs(5, 3);
			mv.visitEnd();
		}

		{
			MethodVisitor mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "main", "([Ljava/lang/String;)V", null, null);
			mv.visitCode();
			mv.visitTypeInsn(NEW, owner);
			mv.visitInsn(DUP);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitInsn(ARRAYLENGTH);
			Label l0 = new Label();
			Label l1 = new Label();
			mv.visitJumpInsn(IFEQ, l0);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitInsn(ICONST_0);
			mv.visitInsn(AALOAD);
			mv.visitJumpInsn(GOTO, l1);
			mv.visitLabel(l0);
			mv.visitLdcInsn("");
			mv.visitLabel(l1);
			mv.visitMethodInsn(INVOKESPECIAL, owner, "<init>", "(Ljava/lang/String;)V");
			mv.visitMethodInsn(INVOKEVIRTUAL, owner, "run", "()V");
			mv.visitInsn(RETURN);
			mv.visitMaxs(5, 2);
			mv.visitEnd();
		}

		// add action methods
		{
			MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "right", "()V", null, null);
			mv.visitCode();
			mv.visitVarInsn(ALOAD, 0);
			mv.visitInsn(ICONST_1);
			mv.visitMethodInsn(INVOKEVIRTUAL, owner, "right", "(I)V");
			mv.visitInsn(RETURN);
			mv.visitMaxs(3, 1);
			mv.visitEnd();
		}

		{
			MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "right", "(I)V", null, null);
			mv.visitCode();
			mv.visitVarInsn(ALOAD, 0);
			mv.visitInsn(DUP);
			mv.visitFieldInsn(GETFIELD, owner, "pos", "I");
			mv.visitVarInsn(ILOAD, 1);
			mv.visitInsn(IADD);
			mv.visitFieldInsn(PUTFIELD, owner, "pos", "I");
			mv.visitInsn(RETURN);
			mv.visitMaxs(3, 2);
			mv.visitEnd();
		}

		{
			MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "left", "()V", null, null);
			mv.visitCode();
			mv.visitVarInsn(ALOAD, 0);
			mv.visitInsn(ICONST_1);
			mv.visitMethodInsn(INVOKEVIRTUAL, owner, "left", "(I)V");
			mv.visitInsn(RETURN);
			mv.visitMaxs(3, 1);
			mv.visitEnd();
		}

		{
			MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "left", "(I)V", null, null);
			mv.visitCode();
			mv.visitVarInsn(ALOAD, 0);
			mv.visitInsn(DUP);
			mv.visitFieldInsn(GETFIELD, owner, "pos", "I");
			mv.visitVarInsn(ILOAD, 1);
			mv.visitInsn(ISUB);
			mv.visitFieldInsn(PUTFIELD, owner, "pos", "I");
			mv.visitInsn(RETURN);
			mv.visitMaxs(3, 2);
			mv.visitEnd();
		}

		{
			MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "add", "()V", null, null);
			mv.visitCode();
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, owner, "data", "[B");
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, owner, "pos", "I");
			mv.visitInsn(DUP2);
			mv.visitInsn(BALOAD);
			mv.visitInsn(ICONST_1);
			mv.visitInsn(IADD);
			mv.visitInsn(I2B);
			mv.visitInsn(BASTORE);
			mv.visitInsn(RETURN);
			mv.visitMaxs(4, 1);
			mv.visitEnd();
		}

		{
			MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "sub", "()V", null, null);
			mv.visitCode();
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, owner, "data", "[B");
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, owner, "pos", "I");
			mv.visitInsn(DUP2);
			mv.visitInsn(BALOAD);
			mv.visitInsn(ICONST_1);
			mv.visitInsn(ISUB);
			mv.visitInsn(I2B);
			mv.visitInsn(BASTORE);
			mv.visitInsn(RETURN);
			mv.visitMaxs(4, 1);
			mv.visitEnd();
		}

		{
			MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "print", "()V", null, null);
			mv.visitCode();
			mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, owner, "data", "[B");
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, owner, "pos", "I");
			mv.visitInsn(BALOAD);
			mv.visitInsn(I2C);
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "print", "(C)V");
			mv.visitInsn(RETURN);
			mv.visitMaxs(3, 1);
			mv.visitEnd();
		}

		{
			MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "read", "()V", null, null);
			mv.visitCode();

			Label l0 = new Label();
			Label l1 = new Label();
			Label l2 = new Label();
			mv.visitTryCatchBlock(l0, l1, l2, "java/io/IOException");

			mv.visitLabel(l0);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, owner, "data", "[B");
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, owner, "pos", "I");
			mv.visitFieldInsn(GETSTATIC, "java/lang/System", "in", "Ljava/io/InputStream;");
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/InputStream", "read", "()I");
			mv.visitInsn(DUP);

			Label l7 = new Label();
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, owner, "newline", "[B");
			mv.visitInsn(ICONST_0);
			mv.visitInsn(BALOAD);
			mv.visitJumpInsn(IF_ICMPNE, l7);

			Label l5 = new Label();
			Label l6 = new Label();
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, owner, "newline", "[B");
			mv.visitInsn(ARRAYLENGTH);
			mv.visitVarInsn(ISTORE, 1);
			mv.visitJumpInsn(GOTO, l6);
			mv.visitLabel(l5);
			mv.visitInsn(POP);
			mv.visitFieldInsn(GETSTATIC, "java/lang/System", "in", "Ljava/io/InputStream;");
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/InputStream", "read", "()I");
			mv.visitIincInsn(1, -1);
			mv.visitLabel(l6);
			mv.visitVarInsn(ILOAD, 1);
			mv.visitJumpInsn(IFGT, l5);

			mv.visitLabel(l7);
			mv.visitInsn(BASTORE);

			mv.visitLabel(l1);

			Label l3 = new Label();
			mv.visitJumpInsn(GOTO, l3);
			mv.visitLabel(l2);
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/IOException", "printStackTrace", "()V");
			mv.visitLabel(l3);

			mv.visitInsn(RETURN);
			mv.visitMaxs(7, 2);
			mv.visitEnd();
		}
	}

	private void begin(MethodVisitor mv) {
		mv.visitVarInsn(ALOAD, 0);
		mv.visitFieldInsn(GETFIELD, owner, "data", "[B");
		mv.visitVarInsn(ALOAD, 0);
		mv.visitFieldInsn(GETFIELD, owner, "pos", "I");
		mv.visitInsn(BALOAD);
		mv.visitInsn(ICONST_0);
		mv.visitJumpInsn(IF_ICMPEQ, labelStack.pop());
		mv.visitLabel(labelStack.pop());
	}

	private void end(MethodVisitor mv) {
		mv.visitVarInsn(ALOAD, 0);
		mv.visitFieldInsn(GETFIELD, owner, "data", "[B");
		mv.visitVarInsn(ALOAD, 0);
		mv.visitFieldInsn(GETFIELD, owner, "pos", "I");
		mv.visitInsn(BALOAD);
		mv.visitInsn(ICONST_0);
		mv.visitJumpInsn(IF_ICMPNE, labelStack.pop());
		mv.visitLabel(labelStack.pop());
	}

	private void finish() {
		cw.visitEnd();
		writeClassFile(cw.toByteArray());
	}

	//	>	increment the data pointer (to point to the next cell to the right).
	//	<	decrement the data pointer (to point to the next cell to the left).
	//	+	increment (increase by one) the byte at the data pointer.
	//	-	decrement (decrease by one) the byte at the data pointer.
	//	.	output the byte at the data pointer.
	//	,	accept one byte of input, storing its value in the byte at the data pointer.
	//	[	if the byte at the data pointer is zero, then instead of moving the instruction pointer forward to the next command, jump it forward to the command after the matching ] command.
	//	]	if the byte at the data pointer is nonzero, then instead of moving the instruction pointer forward to the next command, jump it back to the command after the matching [ command.

	private void writeClassFile(byte[] bytes) {
		FileOutputStream fw;
		try {
			System.out.println("Writing class file: " + classPath + "/" + className + ".class");

			File file = new File(classPath + "/" + className + ".class");
			file.getParentFile().mkdirs();
			fw = new FileOutputStream(file);
			fw.write(bytes);
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
