package com.emathias.jbfc;

import java.io.UnsupportedEncodingException;

/**
 * This class doesn't do anything, I just studied its compiled class to help teach myself java bytecode
 */
public class Empty {
	private byte[] data;
	private int pos;
	private int newlineLength = 2;

	public Empty(String d) {
		try {
			pos = 0;
			data = new byte[30000];
			System.arraycopy(d.getBytes("UTF-8"), 0, data, 0, d.length());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		if (args.length != 0) {
			new Empty(args[0]).run();
		} else {
			new Empty("").run();
		}
	}

	public void run() {
		for (int i = 0; i < newlineLength; i++) {
			System.out.println(i);
		}
	}
}
